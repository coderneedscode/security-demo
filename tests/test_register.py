import unittest
import requests
import sys
from utils import testing_utilities as util

class TestRegister(unittest.TestCase):

    #Tests a successful registration
    def test_database_register_success(self):
        username = "user"
        password = "pass"
        response = util.create_user(username, password)
        util.delete_user(username)

        self.assertEqual(response["result"], "Register successful")


    #Test the case when there is a duplicate registration
    def test_database_register_duplicate(self):
        username = "dup"
        password = "duppass"
        #First register is expected to succeed
        util.create_user(username, password)
        #Second register is expected to fail
        response = util.create_user(username, password)
        util.delete_user(username)

        self.assertEqual(response["result"], "Register unsuccessful")
        self.assertEqual(response["data"]["message"], "Duplicate registration")


    #Test the cases when username or password or both are missing
    def test_database_register_missing_data(self):
        response = requests.post(util.database + "register", data = {}).json()
        self.assertEqual(response["result"], "Register unsuccessful")
        self.assertEqual(response["data"]["message"], "Username or password is not received", "No username or password")

        response = requests.post(util.database + "register", data = {"username": "missing"}).json()
        self.assertEqual(response["result"], "Register unsuccessful")
        self.assertEqual(response["data"]["message"], "Username or password is not received", "No password")

        response = requests.post(util.database + "register", data = {"password": "missing"}).json()
        self.assertEqual(response["result"], "Register unsuccessful")
        self.assertEqual(response["data"]["message"], "Username or password is not received", "No username")

    def test_interface_register(self):
        username = "iTest"
        password = "iPass"
        page = requests.get(util.interface + "register")
        html = page.text
        cookies = page.cookies
        for cookie in cookies:
            if cookie.name == "session":
                session = cookie.value
        csrf_approx_location = html.find('id="csrf_token" name="csrf_token"')
        csrf_location = html.find('>', csrf_approx_location)
        offset = len('id="csrf_token" name="csrf_token" type="hidden" value="')
        csrf_token = html[csrf_approx_location + offset:csrf_location-1]
        
        requests.post(util.interface + "register", data = {"csrf_token": csrf_token,"username": username, "password": password, "password2": password, "submit": "Register"}, cookies={"session": session})
        response = requests.get(util.database + "users").json()
        found = False
        for user in response["data"]["users"]:
            if user["username"] == username:
                found = True
        util.delete_user(username)
        
        self.assertTrue(found)

        
    
