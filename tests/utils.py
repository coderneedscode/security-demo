import requests

class testing_utilities:

    interface = "http://127.0.0.1:5000/"

    database = "http://127.0.0.1:5001/"

    #Creates a user in the database, can make the new user an admin
    def create_user(username, password, admin=False):
        response = requests.post(testing_utilities.database + "register", data={"username": username, "password": password})
        if admin:
            requests.post(database() + "modify", data={"username": username, "admin": True})
        return response.json()

    #Deletes a user from the database
    def delete_user(username):
        response = requests.post(testing_utilities.database + "delete", data={"username": username})
        return response.json()
