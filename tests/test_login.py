import unittest
import requests
import sys
from utils import testing_utilities as util

class TestLogin(unittest.TestCase):

    def test_database_login_success(self):
        #Creates a user in the database
        username = "user"
        password = "pass"
        util.create_user(username, password)
        response = requests.post(util.database + "login", data = {"username": username, "password": password}).json()
        util.delete_user(username)
        self.assertEqual(response["result"], "Authenticated")

    def test_database_login_success_id(self):
        username = "IDuser"
        password = "IDpass"
        util.create_user(username, password)
        #Logs in with username and password to get user's id
        response = requests.post(util.database + "login", data = {"username": username, "password": password}).json()

        #Tests logging in with id
        response = requests.post(util.database + "login", data = {"id": response["data"]["id"]}).json()
        util.delete_user(username)
        self.assertEqual(response["result"], "Authenticated")

    def test_database_login_id_fail(self):
        username = "failID"
        password = "failPass"
        util.create_user(username, password)
        response = requests.post(util.database + "login", data = {"id": "Incorrect ID"}).json()
        util.delete_user(username)
        self.assertEqual(response["result"], "Not authenticated")
        self.assertEqual(response["data"]["message"], "User ID not found")


    def test_database_login_password(self):
        username = "pass"
        password = "word"
        util.create_user(username, password)
        response = requests.post(util.database + "login", data = {"username": username, "password": "Incorrect Password"}).json()
        util.delete_user(username)
        self.assertEqual(response["result"], "Not authenticated")
        self.assertEqual(response["data"]["message"], "Invalid password")

    def test_database_login_missing_data(self):
        username = "missing"
        password = "password"
        util.create_user(username, password)
        responses = []
        responses.append(requests.post(util.database + "login", data = {}).json())

        responses.append(requests.post(util.database + "login", data = {"username": username}).json())

        responses.append(requests.post(util.database + "login", data = {"password": password}).json())

        util.delete_user(username)
        for i in range(len(responses)):
            self.assertEqual(responses[i]["result"], "Not authenticated", "Login Request: " + str(i + 1))
            self.assertEqual(responses[i]["data"]["message"], "Please input username or password", "Login Request: " + str(i + 1))



