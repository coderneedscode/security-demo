import unittest
import requests
import sys
from utils import testing_utilities as util
sys.path.append(sys.path[0][:-5] + "src/InterfaceWebserver/")
from app.routes import send_request

class TestSend_Request(unittest.TestCase):

    #Tests send_request by registering a new user
    def test_send_request_register(self):
        username = "user"
        password = "pass"
        response = send_request(util.database + "register", {"username": username, "password": password})
        util.delete_user(username)
        self.assertEqual(response["result"], "Register successful")

    #Tests send_request by adding a like
    def test_send_request_like(self):
        username = "like"
        password = "pass"
        #Registers test user
        util.create_user(username, password)

        #Likes test user
        response = send_request(util.database + "like", {"username": username})
        util.delete_user(username)
        self.assertEqual(response["result"], "Like successful")

