from flask import Flask

app = Flask(__name__)
app.config['SECRET_KEY'] = 'The-Most-random-string'
app.config['CACHE_TYPE'] = "null"
app.config['UPLOAD_FOLDER'] = "/upload"
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0 # Beneficial for development stage

from flask_login import LoginManager
login = LoginManager(app)
login.login_view == "login"

from flask_bootstrap import Bootstrap
bootstrap = Bootstrap(app)

from app import routes, models

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5000)
