from flask_wtf import FlaskForm
import requests
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectField, HiddenField
from wtforms.validators import DataRequired, EqualTo

class LoginForm(FlaskForm):
    username = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])
    submit = SubmitField("Login")

class RegistrationForm(FlaskForm):
    username = StringField("Username (0-64 Character)", validators=[DataRequired()])
    password = PasswordField("Password (0-64 Character)", validators=[DataRequired()])
    password2 = PasswordField("Repeat Password", validators=[DataRequired(), EqualTo("password")])
    submit = SubmitField("Register")


class SelectForm(FlaskForm):
    user = SelectField('Cat')
    submit = SubmitField('Submit')
