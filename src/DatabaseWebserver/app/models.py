from app import db
from passlib.hash import sha256_crypt
from datetime import datetime
from sqlalchemy.sql import text, expression
from sqlalchemy.exc import OperationalError
from sqlalchemy import Column, Integer, String, Boolean
import os

class User(db.Model):
    id = Column(Integer, primary_key=True)
    username = Column(String(64), index=True, unique=True)
    password = Column(String(64))
    token = Column(String(64), nullable=True)
    picture = Column(String(64), default='default.jpg')
    is_admin = Column(Boolean, server_default=expression.false(), nullable=False)
    like = Column(Integer, default=0)
    __searchable__ = ["username"]

    def create_token(self):
        self.token = sha256_crypt.encrypt(str(datetime.now()))

    def set_picture(self, picture):
        filename = sanitize_username()
        with open(os.path.join("static", filename), "w") as f:
            f.write(picture)
        picture = filename

    def remove_token(self):
        self.token = None

    def set_password(self, password):
        self.password = sha256_crypt.encrypt(password)

    def check_password(self, password):
        return sha256_crypt.verify(password, self.password)

    def __repr__(self):
        return "<User {}>".format(self.username)
