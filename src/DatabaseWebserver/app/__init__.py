from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SECRET_KEY'] = 'The-Most-random-string'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['UPLOAD_FOLDER'] = "/static"
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0 # Beneficial for development stage
db = SQLAlchemy(app)

from app import routes, models

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=5001)