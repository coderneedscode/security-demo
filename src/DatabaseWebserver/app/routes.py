from app import app, db
from flask import render_template, redirect, send_file, request, url_for, jsonify
from app.models import User
import requests
import os
import re

#Is used to confirm server is up
@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
def index():
    return redirect(url_for("admin"))

#Sends the file call resource stored in the static folder
@app.route('/resource/image/<resource>')
def getImage(resource):
    return send_file('static/' + resource) # is this meant to be SSRF hole?

@app.route("/admin", methods=["GET", "POST"])
def admin():
    if 'apiUrl' in request.form:
        requests.post(request.form['apiUrl'], data={key: request.form[key] for key in request.form.keys() if key != 'apiUrl'})


    users = User.query.all()
    return render_template("admin.html", users = users)

#Deletes the given user from the database
@app.route("/delete", methods=["POST"])
def delete():
    response = {}

    if 'username' in request.form:
        user = User.query.filter_by(username=request.form['username']).first()
        if user is not None:
            db.session.delete(user)
            db.session.commit()
            response['result'] = "Delete successful"
    else:
        response['result'] = "Delete unsuccessful"

    return jsonify(response)

#Authenticates the given user using the given password
@app.route("/login", methods=["POST"])
def login():
    response = {}

    if 'username' in request.form and 'password' in request.form:
        user = User.query.filter_by(username=request.form['username']).first()
        if user is not None and user.check_password(request.form['password']):
            response['result'] = "Authenticated"
            response['data'] = {}
            response['data']['id'] = user.id
            response['data']['admin'] = user.is_admin
            response['data']['token'] = user.create_token()
            db.session.commit()
        else:
            response['result'] = "Not authenticated"
            response['data'] = {}
            response['data']['message'] = "Invalid password"
    elif 'id' in request.form:
        user = User.query.filter_by(id=request.form['id']).first()
        if user is not None:
            response['result'] = "Authenticated"
            response['data'] = {}
            response['data']['id'] = user.id
            response['data']['username'] = user.username
            response['data']['admin'] = user.is_admin
            response['data']['token'] = user.token
        else:
            response['result'] = "Not authenticated"
            response['data'] = {}
            response['data']['message'] = "User ID not found"
    else:
        response['result'] = "Not authenticated"
        response['data'] = {}
        response['data']['message'] = "Please input username or password"

    return jsonify(response)

#Authenticates the given user using the given password
@app.route("/logout",methods=["POST"])
def logout():
    response = {}

    if 'username' in request.form:
        user = User.query.filter_by(username=request.form['username']).first()
        if user is not None:
            if user.token is not None:
                user.remove_token()
                db.session.commit()
            else:
                response['data'] = {}
                response['data']['message'] = "User is not logged in before"
            response['result'] = "Logged out"
    else:
        response['result'] = "Not logged out"
        response['data'] = {}
        response['data']['message'] = "Username not found"
    
    return jsonify(response)

#Adds a new user to the database
#Should register and validate methods be combined? Yes
@app.route("/register",methods=["POST"])
def register():
    response = {}

    if 'username' in request.form and 'password' in request.form:
        user = User.query.filter_by(username=request.form['username']).first()
        if user is not None:
            response['result'] = "Register unsuccessful"
            response['data'] = {}
            response['data']['message'] = "Duplicate registration"
        else:
            #Validate data
            if (validate_registration(request.form)):
                user = User(username=request.form['username'])
                user.set_password(request.form['password'])
                db.session.add(user)
                db.session.commit()
                response['result'] = "Register successful"
            else:
                response['result'] = "Register unsuccessful"
                response['data'] = {}
                response['data']['Message'] = "Illegal username or password"
    else:
        response['result'] = "Register unsuccessful"
        response['data'] = {}
        response['data']['message'] = "Username or password is not received"
        
    return jsonify(response)

def validate_registration(data):
    if len(data['username']) > 64 or len(data['password']) > 64:
        return False
    elif re.match("[^A-Za-z0-9]+", data['username']):
        return False
    else:
        return True

#Gives or removes admin privledges
@app.route("/modify",methods=["POST"])
def modify():
    response = {}
    response['result'] = "Modify unsuccessful"
    if request.form:
        if 'username' in request.form:
            username = request.form['username']
            user = User.query.filter_by(username=request.form['username']).first()
            if 'admin' in request.form:
                user.is_admin = eval(request.form['admin'])
                db.session.commit()
                response['result'] = "Modify successful"
    return jsonify(response)

@app.route("/like", methods=["POST", "GET"])
def like():
    if request.method == "POST":
        response = {}
        if 'username' in request.form:
            user = User.query.filter_by(username=request.form['username']).first()
            if user is not None:
                user.like = user.like + 1
                db.session.commit()
                response['result'] = "Like successful"
            else:
                response['result'] = "Like unsuccessful"
                response['data'] = {}
                response['data']['message'] = "Username not found"

        return jsonify(response)

    if request.method == "GET":
        response = {}
        if 'username' in request.args:
            user = User.query.filter_by(username=request.args['username']).first()
            if user is not None:
                response['data'] = {}
                response['data']['like'] = {}
                response['data']['like'] = user.like

        return jsonify(response)

@app.route("/picture", methods=["POST", "GET"])
def picture():
    if request.method == "POST":
        if 'username' in request.form:
            user = User.query.filter_by(username=request.form['username']).first()
            if user is not None:
                file_received = request.files['file']
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                user.picture = filename
                db.session.commit()
    if request.method == "GET":
        if 'username' in request.args:
            user = User.query.filter_by(username=request.args['username']).first()
            if user is not None:
                return send_file(os.path.join("static", user.picture))

#Returns all users usernames
@app.route("/users")
def users():
    response = {}
    response['data'] = {}
    response['data']['users'] = []
    response['data']['pictures'] = {}

    users = User.query.all()

    for user in users:
        response['data']['users'].append({"username" : user.username})
        response['data']['pictures'][user.username] = user.picture

    return jsonify(response)

#Returns the picture used by the user with given username
@app.route("/picture_<username>")
def getUserPicture(username):
    return User.query.filter_by(username=username).first().picture
